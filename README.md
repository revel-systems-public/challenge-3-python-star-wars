# Task description

Write an application that pulls data from api with specific insights about Star Wars characters and shows it in a JSON file.

## Functional requirements
- Application should create a JSON file with list of movie characters
- Application should ask to select a Star Wars character from the list retrieved from the API and then create a JSON file with list of films the selected character was in


## Technical requirements
Use API https://swapi-graphql.netlify.app/.netlify/functions/index/

> Using graphene completely fine

## Bonus
Showcase TDD skills by writing unit tests
